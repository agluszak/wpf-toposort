open PMap;;

exception Cykliczne

let initialize_incoming l = 
    let aux acc (u, vs) = 
        let acc_with_u = add u (ref 0) acc in 
        List.fold_left (fun acc v -> add v (ref 0) acc) acc_with_u vs in 
    List.fold_left (fun acc (u, vs) -> aux acc (u, vs)) empty l

let set_incoming incoming l = 
    List.iter (fun (_, vs) -> List.iter (fun i -> let size = find i incoming in size := !size + 1) vs) l

let build_graph l = 
    let aux_empty acc (u, vs) = 
        let acc_with_u = add u (ref []) acc in
        List.fold_left (fun acc x -> add x (ref []) acc) acc_with_u vs in 
    let graph = List.fold_left (fun acc (u, vs) -> aux_empty acc (u, vs) ) empty l in 
    List.iter (fun (u, vs) -> let vert = find u graph in vert := vs @ !vert) l;
    graph
    
let topol l = 
    let incoming = initialize_incoming l in
    set_incoming incoming l;
    let graph = build_graph l in
    let sorted = ref [] in 
    let set = ref (foldi (fun v size acc -> if !size = 0 then v::acc else acc) incoming []) in 
    while !set <> [] do 
        let (n, tail) = (List.hd !set, List.tl !set) in
        sorted := n::(!sorted);
        set := tail;
        List.iter (fun u -> let size = find u incoming in size := !size - 1; if !size = 0 then set := u::(!set) ) !(find n graph);
    done;
    if fold (fun size acc -> !size = 0 && acc) incoming true then
        List.rev !sorted
    else 
        raise Cykliczne